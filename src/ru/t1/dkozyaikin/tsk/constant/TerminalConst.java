package ru.t1.dkozyaikin.tsk.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

}
