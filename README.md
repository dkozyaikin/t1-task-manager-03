# task-manager

## developer
**Denis Kozyaikin**

**dkozyaikin@t1-consulting.ru**

## description

**Проект по заданию JSE-03 "ВВЕДЕНИЕ В DVS GIT" обучающего курса JAVA разработки**

## software

- **Java**: OpenJDK 17
- **OS**: macOS Big Sur

## hardware

- **CPU**: Intel Core i7 2,3 GHz
- **RAM**: 16Gb DDR4 3733 MHz

## run application

`java -jar ./out/artifacts/task_manager/task-manager.jar version`
